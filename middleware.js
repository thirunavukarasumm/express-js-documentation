const express = require("express");

const app = express();

const morgan = require("morgan");

const { products, people } = require("./data");

const logger = require("./logger");

const authorize = require("./authorize");

const PORT = 8080;

app.use(morgan("default"))

// app.use([authorize,logger]) // This applies the logger for all the users but the placement of this is important.

app.disable('etag');

app.get("/", (req, res) => {
    res.send("Home")
})

app.get("/api/about", (req, res) => {
    res.send("About")
})

app.get("/api/above", (req, res) => {
    res.send("About")
})

app.listen(PORT, () => {
    console.log(`The server is listening on port - ${PORT}`);
})
