const express = require("express");

const app = express();

const { products } = require("./data");

const PORT = 8080;

app.disable('etag');

app.get("/", (req, res) => {
    return res.send("<h1>Home Page</h1><a href='./api/products/'>Products</a>");
})

app.get("/api/products", (req, res) => {
    const newProducts = products.map((product) => {
        const { id, name, image } = product;
        return { id, name, image };
    })
    return res.json(newProducts);
    // res.json({ad:"svd"});
})


app.get("/api/products/:id", (req, res) => {
    const singleProduct = products.find((product) => {
        const { id } = req.params;
        return product["id"] == id;
    })
    if (!singleProduct) {
        return res.status(404).json({ message: "An item with the passed ID is not available." })
    } else {
        return res.status(200).json(singleProduct);
    }
})

// api/v1/query?search=a&limit=2

app.get("/api/v1/query", (req, res) => {
    const { search, limit } = req.query;
    let sortedProducts = [...products];
    if (search) {
        sortedProducts = products.filter((product) => {
            return product["name"].includes(search);
        })
    }
    if (limit) {
        sortedProducts = sortedProducts.slice(0, Number(limit));
    }
    if (sortedProducts.length === 0) {
        // res.status(200).send("No products matched your search.")
        return res.status(200).json({ success: true, data: [] });
    }
    return res.status(200).json(sortedProducts);


})
app.listen(PORT, () => {
    console.log(`The server is listening on port - ${PORT}`);
})
