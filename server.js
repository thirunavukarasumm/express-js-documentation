const express = require("express");

const app = express();

const morgan = require("morgan")("default");

const { people } = require("./data");

const PORT = 8080;

app.use(express.static("./methods-public"))

app.use(express.urlencoded({ extended: false }));

app.use(morgan)

app.disable('etag');

app.get("/api/people", (req, res) => {
    res.status(200).json({ success: true, data: people });
})

app.post("/login", (req, res) => {
    const { name } = req.body;
    if(name) {
        const userStatus = people.find((user)=>{
            return user["name"] === name;
        })
        if(userStatus){
            return res.status(200).send(`Welcome ${name}`);

        } else {
            people.push({
                name,
                id: people.length
            })
            return res.status(200).send(`Welcome ${name}, you are added to the database.`);

        }
    } else {
        return res.status(401).send(`Username can't be empty.`);

    }
})

app.listen(PORT, () => {
    console.log(`The server is listening on port - ${PORT}`);
})
