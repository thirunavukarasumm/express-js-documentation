const express = require("express");

const app = express();

const { products } = require("./data");

const PORT = 8080;

app.get("/", (req, res) => {
    res.json(products)
})

app.listen(PORT, () => {
    console.log(`The server is listening on port - ${PORT}`);
})
