const express = require("express");

const path = require("path");

const app = express();

app.use(express.static("./public"));

const PORT = 8080;

// app.get("/", (req, res) => {
//     res.status(200).sendFile(path.resolve(__dirname,"./public/index.html"))
// })

app.get("/about", (req, res) => {
    res.status(200).send("About Page")
})

app.all("*",(req,res)=>{
    res.status(404).send("<h1>Resource not found.</h1>")
})

app.listen(PORT, () => {
    console.log(`The server is listening on port - ${PORT}`);
})

// app.get
// app.post
// app.put
// app.delete
// app.all
// app.use
// app.get
