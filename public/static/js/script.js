function fetchData(API) {
    return fetch(API)
        .then((data) => {
            if (data.ok) {
                return data.json();
            } else {
                throw `${data.statusText} ${data.status}`;
            }
        });
}

function groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
        if (acc[item.category]) {
            acc[item.category].push(item);
        } else {
            acc[item.category] = [item];
        }
        return acc;
    }, {});
}

function createAElement(tag, ClassName, content) {
    let div = document.createElement(tag);
    div.classList.add(ClassName);
    if (content) {
        div.innerHTML = content;
    }
    return div;
}

function populateTemplate(API) {
    fetchData(API)
        .then((items) => {
            return groupDataBasedOnCategory(items);
        })
        .then((groupedObjects) => {
            let section = document.getElementsByClassName("section-one-part-two");
            for (let key in groupedObjects) {
                let titleContent = `<div class="title">
                                        <h1>${key}</h1>
                                    </div>`
                let wrapDiv = createAElement("div", "wrap", titleContent);
                let content = createAElement("div", "contents")
                groupedObjects[key].forEach((item) => {
                    let cardContent = `
                        <img src=${item.image} alt="" srcset="">
                        <div class="content-spacing">
                            <h3>${item.title}</h3>
                            <h4>$${item.price}</h4>
                            <p> ${item.description}</p>
                            <h4> Rating - ${item.rating["rate"]} | Available pieces - ${item.rating["count"]} </h4>
                            <button> Add to cart </button>
                        </div>
                    `
                    let card = createAElement("div", "card", cardContent);
                    content.appendChild(card)
                })
                wrapDiv.appendChild(content)
                section[0].appendChild(wrapDiv)
            }

        })
        .catch((err) => {
            let section = document.getElementsByClassName("section-one-part-two");
            let errorContent = "";
            if (typeof err === "string") {
                errorContent = `<div class="title">
                                        <h1>${err}</h1>
                                    </div>`
            } else {
                errorContent = `<div class="title">
                                        <h1>Error while fetching the API.</h1>
                                    </div>`
            }
            let wrapDiv = createAElement("div", "wrap", errorContent);
            section[0].appendChild(wrapDiv)


        })


}

const API = "https://fakestoreapi.com/products";
populateTemplate(API);